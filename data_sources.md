# Open Datasets/Resources

## End-to-end datasets with vehicle data

1. [Davis Driving Dataset](https://docs.google.com/document/d/1HM0CSmjO8nOpUeTvmPjopcBcVCk7KXvLUuiZFS6TWSg/pub)

2. [DBNet - Large Scale Driving Behavior](http://www.dbehavior.net/)

3. [UAH Dataset](http://www.robesafe.uah.es/personal/eduardo.romera/uah-driveset/)

4. [Berkeley DeepDrive](https://bdd-data.berkeley.edu/) 

## Sensor/Image recognition datasets

1. Astyx Dataset HiRes2019 

2. Waymo Open Dataset

3. nuScenes 

4. Open Images v5
